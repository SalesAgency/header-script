//<script>

const qString = new URLSearchParams(window.location.search);
var id;
var siteid;
var ipData = {};

const link_id = qString.get('id');
const sitd = qString.get('siteId');


async function fetchLambda(urlAdd) {
  var newId;
  (async () => {
    return await fetch('https://api.smartdataprocessor.com/findLinkId?' + urlAdd, { method: 'GET' })
      .then(response => {
        if (response.ok) {
          const data = response.json();
          return data.body.linkId;
        } else {
          return {};
        }
      })
  })
  return newId;
}


if (qString.has('id') || qString.has('mbid') || qString.has('tambid') || qString.has('sambid')) {
  if (qString.has('id')) {
    id = qString.get('id');
    localStorage.linkId = id;
  }
  else if (qString.has('mbid')) {
    id = fetchLambda("mbid=" + qString.get('mbid'));
    localStorage.linkId = id;
  }
  else if (qString.has('tambid')) {
    id = fetchLambda("tambid=" + qString.get('tambid'));
    localStorage.linkId = id;
  }
  else if (qString.has('sambid')) {
    id = fetchLambda("sambid=" + qString.get('sambid'));
    localStorage.linkId = id;
  }
  else {
    id = 'TA638';
    localStorage.linkId = 'TA638';
  }
}
else if (localStorage.id || localStorage.mbid || localStorage.sambid || localStorage.tambid) {
  if (localStorage.id) {
    localStorage.linkId = localStorage.id;
  }
  else if (localStorage.mbid) {
    id = fetchLambda("mbid=" + localStorage.mbid);
    localStorage.linkId = id;
  }
  else if (localStorage.sambid) {
    id = fetchLambda("sambid=" + localStorage.sambid);
    localStorage.linkId = id;
  }
  else if (localStorage.tambid) {
    id = fetchLambda("tambid=" + localStorage.tambid);
    localStorage.linkId = id;
  }
  else {
    id = "TA638";
    localStorage.linkId = id;
  }
}
if (id == null || id == undefined) {
  id = "TA638";
  localStorage.linkId = id;
}
if (sitd) {
  siteid = sitd;
  localStorage.siteId = sitd;
}
else {
  siteid = 'null';
}

(async () => {
  return await fetch('https://ipapi.co/json/', { method: 'GET' })
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        return {};
      }
    })
})().then(res => {
  ipData = res;
});

//</script>